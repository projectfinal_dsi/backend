import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Branch } from './branch/entities/branch.entity';
import { BranchModule } from './branch/branch.module';
import { DataSource } from 'typeorm';
import { Employee } from './employee/entities/employee.entity';
import { EmployeeModule } from './employee/employee.module';
import { RolesModule } from './roles/roles.module';
import { Role } from './roles/entities/role.entity';
import { CustomerModule } from './customer/customer.module';
import { Customer } from './customer/entities/customer.entity';
import { ProductModule } from './product/product.module';
import { Product } from './product/entities/product.entity';
import { ProductcategoryModule } from './productcategory/productcategory.module';
import { Productcategory } from './productcategory/entities/productcategory.entity';
import { PromotionModule } from './promotion/promotion.module';
import { Promotion } from './promotion/entities/promotion.entity';
import { ImportMaterial } from './import/entities/import.entity';
import { importModule } from './import/import.module';
import { CheckinoutModule } from './checkinout/checkinout.module';
import { Checkinout } from './checkinout/entities/checkinout.entity';
import { InvoiceModule } from './invoice/invoice.module';
import { Invoice } from './invoice/entities/invoice.entity';
import { InvoiceDetail } from './invoice/entities/invoiceDetail.entity';
import { ReceiptModule } from './receipt/receipt.module';
import { Receipt } from './receipt/entities/receipt.entity';
import { ReceiptDetail } from './receipt/entities/receiptDetail.entity';
import { BranchStock } from './branchstock/entities/branchstock.entity';
import { BranchStockModule } from './branchstock/branchstock.module';
import { MaterialModule } from './material/material.module';
import { Material } from './material/entities/material.entity';
import { CheckStock } from './checkstock/entities/checkstock.entity';
import { CheckStockModule } from './checkstock/checkstock.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'database.sqlite',
      synchronize: true,
      logging: false,
      entities: [
        Branch,
        Employee,
        Role,
        Customer,
        Product,
        Productcategory,
        Promotion,
        ImportMaterial,
        Checkinout,
        Invoice,
        InvoiceDetail,
        BranchStock,
        Receipt,
        ReceiptDetail,
        Material,
        CheckStock,
      ],
    }),
    BranchModule,
    EmployeeModule,
    RolesModule,
    CustomerModule,
    ProductModule,
    ProductcategoryModule,
    PromotionModule,
    importModule,
    CheckinoutModule,
    InvoiceModule,
    ReceiptModule,
    BranchStockModule,
    MaterialModule,
    CheckStockModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
