import { Module } from '@nestjs/common';
import { BranchesService } from './branch.service';
import { BranchController } from './branch.controller';
import { Branch } from './entities/branch.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Employee } from 'src/employee/entities/employee.entity';
import { Invoice } from 'src/invoice/entities/invoice.entity';
import { Receipt } from 'src/receipt/entities/receipt.entity';
@Module({
  imports: [TypeOrmModule.forFeature([Branch, Employee, Invoice, Receipt])],
  controllers: [BranchController],
  providers: [BranchesService],
})
export class BranchModule {}
