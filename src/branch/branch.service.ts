/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { CreateBranchDto } from './dto/create-branch.dto';
import { UpdateBranchDto } from './dto/update-branch.dto';
import { Branch } from './entities/branch.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class BranchesService {
  constructor(
    @InjectRepository(Branch)
    private branchRepository: Repository<Branch>,
  ) {}

  create(createBranchDto: CreateBranchDto): Promise<Branch> {
    return this.branchRepository.save(createBranchDto);
  }

  findAll(): Promise<Branch[]> {
    return this.branchRepository.find();
  }

  findOne(id: number) {
    return this.branchRepository.findOneBy({ branchId: id });
  }

  async update(branchId: number, updateBranchDto: UpdateBranchDto) {
    await this.branchRepository.update(branchId, updateBranchDto);
    const Branch = await this.branchRepository.findOneBy({ branchId });
    return Branch;
  }

  async remove(branchId: number) {
    const deleteBranch = await this.branchRepository.findOneBy({ branchId });
    return this.branchRepository.remove(deleteBranch);
  }
}
