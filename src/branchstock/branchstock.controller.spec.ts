import { Test, TestingModule } from '@nestjs/testing';
import { BranchStockController } from './branchstock.controller';
import { BranchStockService } from './branchstock.service';

describe('branchstock', () => {
  let controller: BranchStockController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BranchStockController],
      providers: [BranchStockService],
    }).compile();

    controller = module.get<BranchStockController>(BranchStockController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
