import { IsNotEmpty } from 'class-validator';

export class CreateBranchStockDto {
  @IsNotEmpty()
  qty: number;

  @IsNotEmpty()
  branch: { branchId: number };
}
