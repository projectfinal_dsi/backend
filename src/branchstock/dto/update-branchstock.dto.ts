import { PartialType } from '@nestjs/swagger';
import { CreateBranchStockDto } from './create-branchstock.dto';

export class UpdateBranchStockDto extends PartialType(CreateBranchStockDto) {}
