import { Branch } from 'src/branch/entities/branch.entity';
// import { Material } from 'src/employee/entities/material.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
@Entity()
export class BranchStock {
  @PrimaryGeneratedColumn()
  branchStockId: number;

  @Column()
  qty: number;

  @ManyToOne(() => Branch, (branch) => branch.branchStock)
  branch: Branch;

  // @ManyToOne(() => Material, (material) => material.BranchStock)
  // material: Material;
}
