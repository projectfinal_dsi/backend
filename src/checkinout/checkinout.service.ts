import { Injectable } from '@nestjs/common';
import { CreateCheckinoutDto } from './dto/create-checkinout.dto';
import { UpdateCheckinoutDto } from './dto/update-checkinout.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Checkinout } from './entities/checkinout.entity';
import { DeepPartial, Repository } from 'typeorm';
import { Employee } from 'src/employee/entities/employee.entity';

@Injectable()
export class CheckinoutService {
  constructor(
    @InjectRepository(Checkinout)
    private checkinoutRepository: Repository<Checkinout>,
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
  ) {}
  async create(createCheckinoutDto: CreateCheckinoutDto) {
    const checkIn = new Checkinout();
    checkIn.checkIn = createCheckinoutDto.checkIn;
    checkIn.checkOut = createCheckinoutDto.checkOut;
    checkIn.timeWork = createCheckinoutDto.timeWork;
    checkIn.status = createCheckinoutDto.status;
    const employee = await this.employeeRepository.findOneBy({
      employeeId: createCheckinoutDto.employee.employeeId,
    });
    checkIn.employee = employee;
    return this.checkinoutRepository.save(checkIn);
  }

  findAll() {
    return this.checkinoutRepository.find({ relations: ['employee'] });
  }

  findOne(checkInOutId: number) {
    return this.checkinoutRepository.findOne({
      where: { checkInOutId },
      relations: ['employee'],
    });
  }

  async update(checkInOutId: number, updateCheckinoutDto: UpdateCheckinoutDto) {
    const updateCheckinOut: DeepPartial<Checkinout> = {
      checkInOutId: checkInOutId,
      checkIn: updateCheckinoutDto.checkIn,
      checkOut: updateCheckinoutDto.checkOut,
      timeWork: updateCheckinoutDto.timeWork,
      status: updateCheckinoutDto.status,
      employee: {},
    };

    if (updateCheckinoutDto.employee) {
      const employee = await this.employeeRepository.findOne({
        where: { employeeId: updateCheckinoutDto.employee.employeeId },
      });
      if (employee) {
        updateCheckinOut.employee = employee;
      } else {
        throw new Error(
          `Employee with ID ${updateCheckinoutDto.employee.employeeId} not found`,
        );
      }
    }
    await this.checkinoutRepository.update(checkInOutId, updateCheckinOut);
    return this.checkinoutRepository.findOne({
      where: { checkInOutId },
      relations: ['employee'],
    });
  }

  async remove(checkInOutId: number) {
    const removeCheckInOut = await this.checkinoutRepository.findOneBy({
      checkInOutId,
    });
    return this.checkinoutRepository.remove(removeCheckInOut);
  }
}
