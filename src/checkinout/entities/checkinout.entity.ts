import { Employee } from 'src/employee/entities/employee.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Checkinout {
  @PrimaryGeneratedColumn()
  checkInOutId: number;

  @Column()
  checkIn: string;

  @Column({ nullable: true })
  checkOut: string;

  @Column({ type: 'decimal', precision: 10, scale: 2, nullable: true })
  timeWork: number;

  @Column({ nullable: true })
  status: number;

  @ManyToOne(() => Employee, (employee) => employee.checkinout)
  employee: Employee;
}
