import { Module } from '@nestjs/common';
import { CheckStockService } from './checkstock.service';
import { CheckStockController } from './checkstock.controller';
import { CheckStock } from './entities/checkstock.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Employee } from 'src/employee/entities/employee.entity';
import { Branch } from 'src/branch/entities/branch.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CheckStock, Employee, Branch])],
  controllers: [CheckStockController],
  providers: [CheckStockService],
})
export class CheckStockModule {}
