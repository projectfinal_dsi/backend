import { Test, TestingModule } from '@nestjs/testing';
import { CheckStockService } from './checkstock.service';

describe('checkStockService', () => {
  let service: CheckStockService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CheckStockService],
    }).compile();

    service = module.get<CheckStockService>(CheckStockService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
