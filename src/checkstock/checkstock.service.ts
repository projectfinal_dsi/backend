import { Injectable } from '@nestjs/common';
import { CreateCheckStockDto } from './dto/create-checkstock.dto';
import { UpdateCheckStockDto } from './dto/update-checkstock.dto';
import { CheckStock } from './entities/checkstock.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Employee } from 'src/employee/entities/employee.entity';
import { Branch } from 'src/branch/entities/branch.entity';

@Injectable()
export class CheckStockService {
  constructor(
    @InjectRepository(CheckStock)
    private CheckStockRepository: Repository<CheckStock>,
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
    @InjectRepository(Branch)
    private branchRepository: Repository<Branch>,
  ) {}

  async create(createCheckStockDto: CreateCheckStockDto) {
    const checkStock = new CheckStock();
    checkStock.date = createCheckStockDto.date;
    const employee = await this.employeeRepository.findOneBy({
      employeeId: createCheckStockDto.employee.employeeId,
    });
    const branch = await this.branchRepository.findOneBy({
      branchId: createCheckStockDto.branch.branchId,
    });
    checkStock.employee = employee;
    checkStock.branch = branch;

    return this.CheckStockRepository.save(checkStock);
  }

  findAll(): Promise<CheckStock[]> {
    return this.CheckStockRepository.find({
      relations: ['employee', 'branch'],
    });
  }

  findOne(checkStockId: number) {
    return this.CheckStockRepository.findOne({
      where: { checkStockId },
      relations: ['employee', 'branch'],
    });
  }

  async update(id: number, updateCheckStockDto: UpdateCheckStockDto) {
    return 'not use Return';
  }

  async remove(checkStockId: number) {
    const deletecheckStock = await this.CheckStockRepository.findOneBy({
      checkStockId,
    });
    return this.CheckStockRepository.remove(deletecheckStock);
  }
}
