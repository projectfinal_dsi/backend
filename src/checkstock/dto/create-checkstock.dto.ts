import { IsNotEmpty } from 'class-validator';

export class CreateCheckStockDto {
  @IsNotEmpty()
  date: string;

  @IsNotEmpty()
  employee: { employeeId: number };

  @IsNotEmpty()
  branch: { branchId: number };
}
