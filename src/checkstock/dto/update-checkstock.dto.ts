import { PartialType } from '@nestjs/swagger';
import { CreateCheckStockDto } from './create-checkstock.dto';

export class UpdateCheckStockDto extends PartialType(CreateCheckStockDto) {}
