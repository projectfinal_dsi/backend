import { Branch } from 'src/branch/entities/branch.entity';
import { Employee } from 'src/employee/entities/employee.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
@Entity()
export class CheckStock {
  @PrimaryGeneratedColumn()
  checkStockId: number;

  @Column()
  date: string;

  @ManyToOne(() => Employee, (employee) => employee.importMaterial)
  employee: Employee;

  @ManyToOne(() => Branch, (branch) => branch.importMaterial)
  branch: Branch;
}
