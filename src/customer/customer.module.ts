import { Module } from '@nestjs/common';
import { CustomerService } from './customer.service';
import { CustomerController } from './customer.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Customer } from './entities/customer.entity';
import { Receipt } from 'src/receipt/entities/receipt.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Customer, Receipt])],
  controllers: [CustomerController],
  providers: [CustomerService],
})
export class CustomerModule {}
