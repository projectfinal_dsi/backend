import { Injectable } from '@nestjs/common';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Customer } from './entities/customer.entity';

@Injectable()
export class CustomerService {
  constructor(
    @InjectRepository(Customer)
    private CustomerRepository: Repository<Customer>,
  ) {}
  create(createCustomerDto: CreateCustomerDto) {
    return this.CustomerRepository.save(createCustomerDto);
  }

  findAll() {
    return this.CustomerRepository.find();
  }

  findOne(id: number) {
    return this.CustomerRepository.findOneBy({ customerId: id });
  }

  async update(customerId: number, updateCustomerDto: UpdateCustomerDto) {
    await this.CustomerRepository.update(customerId, updateCustomerDto);
    const Customer = await this.CustomerRepository.findOneBy({ customerId });
    return Customer;
  }

  async remove(customerId: number) {
    const deleteCustomer = await this.CustomerRepository.findOneBy({
      customerId,
    });
    return this.CustomerRepository.remove(deleteCustomer);
  }
}
