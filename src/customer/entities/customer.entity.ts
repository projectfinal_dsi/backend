import { Receipt } from 'src/receipt/entities/receipt.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Customer {
  @PrimaryGeneratedColumn()
  customerId: number;

  @Column()
  name: string;

  @Column()
  phone: string;

  @Column()
  birthDate: string;

  @Column()
  point: number;

  @OneToMany(() => Receipt, (receipt) => receipt.customer)
  receipt: Receipt[];
}
