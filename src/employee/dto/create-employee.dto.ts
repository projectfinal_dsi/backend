import { IsNotEmpty, IsOptional } from 'class-validator';

export class CreateEmployeeDto {
  @IsNotEmpty()
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  email: string;
  @IsNotEmpty()
  password: string;
  @IsNotEmpty()
  roles: { roleId: number }[];
  @IsNotEmpty()
  gender: string;
  @IsNotEmpty()
  salaryType: string;
  @IsOptional()
  salary: number;
  @IsNotEmpty()
  phone: string;
  @IsNotEmpty()
  wagePerHour: number;
  @IsNotEmpty()
  branch: { branchId: number }[];
}
