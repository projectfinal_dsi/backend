import { Injectable } from '@nestjs/common';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { Employee } from './entities/employee.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Branch } from 'src/branch/entities/branch.entity';
import { Repository } from 'typeorm';
import { Role } from 'src/roles/entities/role.entity';

@Injectable()
export class EmployeeService {
  constructor(
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
    @InjectRepository(Branch)
    private branchRepository: Repository<Branch>,
    @InjectRepository(Role)
    private roleRepository: Repository<Role>,
  ) {}
  async create(createEmployeeDto: CreateEmployeeDto) {
    const employee = new Employee();
    employee.name = createEmployeeDto.name;
    employee.email = createEmployeeDto.email;
    employee.password = createEmployeeDto.password;
    employee.roles = [];
    employee.gender = createEmployeeDto.gender;
    employee.salaryType = createEmployeeDto.salaryType;
    employee.salary = createEmployeeDto.salary;
    employee.phone = createEmployeeDto.phone;
    employee.wagePerHour = createEmployeeDto.wagePerHour;
    employee.branch = [];

    for (const role of createEmployeeDto.roles) {
      const r = role.roleId;
      if (r) {
        const role = await this.roleRepository.findOne({
          where: { roleId: r },
        });
        employee.roles.push(role);
      } else {
        // Handle case where branch is not found
        // You can choose to log an error or handle it as per your application's requirement
        console.error(`Branch with id ${role} not found.`);
      }
    }

    for (const branch of createEmployeeDto.branch) {
      const b = branch.branchId;
      if (b) {
        const branch = await this.branchRepository.findOne({
          where: { branchId: b },
        });
        employee.branch.push(branch);
      } else {
        // Handle case where branch is not found
        // You can choose to log an error or handle it as per your application's requirement
        console.error(`Branch with id ${branch} not found.`);
      }
    }

    return this.employeeRepository.save(employee);
  }

  findAll() {
    return this.employeeRepository.find({ relations: ['branch', 'roles'] });
  }

  findOne(employeeId: number) {
    return this.employeeRepository.findOne({
      where: { employeeId },
      relations: ['branch', 'roles'],
    });
  }

  async update(employeeId: number, updateEmployeeDto: UpdateEmployeeDto) {
    return 'Kuay';
  }

  async remove(employeeId: number) {
    const removeEmployee = await this.employeeRepository.findOne({
      where: { employeeId },
    });
    return this.employeeRepository.remove(removeEmployee);
  }
}
