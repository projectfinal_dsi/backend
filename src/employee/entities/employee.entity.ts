import { Branch } from 'src/branch/entities/branch.entity';
import { Checkinout } from 'src/checkinout/entities/checkinout.entity';
import { ImportMaterial } from 'src/import/entities/import.entity';
import { Invoice } from 'src/invoice/entities/invoice.entity';
import { Receipt } from 'src/receipt/entities/receipt.entity';
import { Role } from 'src/roles/entities/role.entity';
import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Employee {
  @PrimaryGeneratedColumn()
  employeeId: number;

  @Column()
  name: string;

  @Column()
  email: string;

  @Column()
  password: string;

  @ManyToMany(() => Role, (role) => role.employees)
  @JoinTable()
  roles: Role[];

  @Column()
  gender: string;

  @Column()
  salaryType: string;

  @Column()
  salary?: number;

  @Column()
  phone: string;

  @Column()
  wagePerHour: number;

  @ManyToMany(() => Branch, (branch) => branch.employees)
  @JoinTable()
  branch: Branch[];

  @OneToMany(() => Checkinout, (checkinout) => checkinout.employee)
  checkinout: Checkinout[];

  @OneToMany(() => Invoice, (invoice) => invoice.employee)
  invoices: Invoice[];

  @OneToMany(() => ImportMaterial, (importMaterial) => importMaterial.employee)
  importMaterial: ImportMaterial[];

  @OneToMany(() => Receipt, (receipt) => receipt.employee)
  receipt: Receipt[];
}
