import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ImportMaterialService } from './import.service';
import { CreateImportDto } from './dto/create-import.dto';
import { UpdateImportDto } from './dto/update-import.dto';

@Controller('import')
export class ImportController {
  constructor(private readonly importMaterialService: ImportMaterialService) {}

  @Post()
  create(@Body() createImportDto: CreateImportDto) {
    return this.importMaterialService.create(createImportDto);
  }

  @Get()
  findAll() {
    return this.importMaterialService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.importMaterialService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateImportDto: UpdateImportDto) {
    return this.importMaterialService.update(+id, updateImportDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.importMaterialService.remove(+id);
  }
}
