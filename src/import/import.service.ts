import { Injectable } from '@nestjs/common';
import { CreateImportDto } from './dto/create-import.dto';
import { UpdateImportDto } from './dto/update-import.dto';
import { ImportMaterial } from './entities/import.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Employee } from 'src/employee/entities/employee.entity';
import { Branch } from 'src/branch/entities/branch.entity';

@Injectable()
export class ImportMaterialService {
  constructor(
    @InjectRepository(ImportMaterial)
    private ImportRepository: Repository<ImportMaterial>,
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
    @InjectRepository(Branch)
    private branchRepository: Repository<Branch>,
  ) {}

  async create(createImportDto: CreateImportDto) {
    const importMaterial = new ImportMaterial();
    importMaterial.date = createImportDto.date;
    importMaterial.total = createImportDto.total;
    importMaterial.totalList = createImportDto.totalList;
    const employee = await this.employeeRepository.findOneBy({
      employeeId: createImportDto.employee.employeeId,
    });
    const branch = await this.branchRepository.findOneBy({
      branchId: createImportDto.branch.branchId,
    });
    importMaterial.employee = employee;
    importMaterial.branch = branch;

    return this.ImportRepository.save(importMaterial);
  }

  findAll(): Promise<ImportMaterial[]> {
    return this.ImportRepository.find({
      relations: ['employee', 'branch'],
    });
  }

  findOne(importMaterialId: number) {
    return this.ImportRepository.findOne({
      where: { importMaterialId },
      relations: ['employee', 'branch'],
    });
  }

  async update(id: number, updateImportDto: UpdateImportDto) {
    return 'not use Return';
  }

  async remove(importMaterialId: number) {
    const deleteimportMaterial = await this.ImportRepository.findOneBy({
      importMaterialId,
    });
    return this.ImportRepository.remove(deleteimportMaterial);
  }
}
