import { IsNotEmpty, IsOptional } from 'class-validator';

export class CreateInvoiceDto {
  @IsNotEmpty()
  type: string;

  @IsNotEmpty()
  total: number;

  @IsNotEmpty()
  paymentState: string;

  @IsOptional()
  datePay: string;

  @IsNotEmpty()
  branch: { branchId: number };

  @IsNotEmpty()
  employee: { employeeId: number };

  @IsNotEmpty()
  invoiceDetails: {
    invoiceDetailid?: number;
    item: string;
    qty: number;
    price: number;
    total: number;
    unit: string;
  }[];
}
