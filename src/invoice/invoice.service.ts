import { Injectable } from '@nestjs/common';
import { CreateInvoiceDto } from './dto/create-invoice.dto';
// import { UpdateInvoiceDto } from './dto/update-invoice.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Invoice } from './entities/invoice.entity';
import { Repository } from 'typeorm';
import { Employee } from 'src/employee/entities/employee.entity';
import { Branch } from 'src/branch/entities/branch.entity';
import { InvoiceDetail } from './entities/invoiceDetail.entity';

@Injectable()
export class InvoiceService {
  constructor(
    @InjectRepository(Invoice)
    private invoiceRepository: Repository<Invoice>,
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
    @InjectRepository(Branch)
    private branchRepository: Repository<Branch>,
    @InjectRepository(InvoiceDetail)
    private invoiceDetailRepository: Repository<InvoiceDetail>,
  ) {}
  async create(createInvoiceDto: CreateInvoiceDto) {
    const invoice = new Invoice();
    invoice.type = createInvoiceDto.type;
    invoice.total = createInvoiceDto.total;
    invoice.paymentState = createInvoiceDto.paymentState;
    invoice.datePay = createInvoiceDto.datePay;
    const employee = await this.employeeRepository.findOneBy({
      employeeId: createInvoiceDto.employee.employeeId,
    });
    const branch = await this.branchRepository.findOneBy({
      branchId: createInvoiceDto.branch.branchId,
    });
    invoice.employee = employee;
    invoice.branch = branch;
    invoice.invoiceDetails = [];
    for (const invoiceDetail of createInvoiceDto.invoiceDetails) {
      const newIVD = new InvoiceDetail();
      newIVD.item = invoiceDetail.item;
      newIVD.qty = invoiceDetail.qty;
      newIVD.price = invoiceDetail.price;
      newIVD.total = invoiceDetail.total;
      newIVD.unit = invoiceDetail.unit;
      await this.invoiceDetailRepository.save(newIVD);
      invoice.invoiceDetails.push(newIVD);
    }
    return this.invoiceRepository.save(invoice);
  }

  findAll() {
    return this.invoiceRepository.find({
      relations: ['employee', 'branch', 'invoiceDetails'],
    });
  }

  findOne(invoiceId: number) {
    return this.invoiceRepository.findOne({
      where: { invoiceId },
      relations: ['employee', 'branch', 'invoiceDetails'],
    });
  }

  // async update(id: number, updateInvoiceDto: UpdateInvoiceDto) {

  //   // const updateInvoice: DeepPartial<Invoice> = {
  //   //   type: updateInvoiceDto.type,
  //   //   total: updateInvoiceDto.total,
  //   //   paymentState: updateInvoiceDto.paymentState,
  //   //   datePay: updateInvoiceDto.datePay,
  //   //   branch: {}, // ป้องกันการเกิดข้อกผิดพลาด
  //   //   employee: {}, // ป้องกันการเกิดข้อผิดพลาด
  //   // };
  //   // const employee = await this.employeeRepository.findOneBy({
  //   //   id: updateInvoiceDto.employee.employeeId,
  //   // });
  //   // const branch = await this.branchRepository.findOneBy({
  //   //   id: updateInvoiceDto.branch.branchId,
  //   // });
  //   // updateInvoice.employee = employee;
  //   // updateInvoice.branch = branch;
  //   // for (const invoiceDetail of updateInvoiceDto.invoiceDetails) {
  //   //   const updateIVDetail: DeepPartial<InvoiceDetail> = {
  //   //     id: invoiceDetail.id,
  //   //     item: invoiceDetail.item,
  //   //     qty: invoiceDetail.qty,
  //   //     price: invoiceDetail.price,
  //   //     total: invoiceDetail.total,
  //   //     unit: invoiceDetail.unit,
  //   //   };
  //   //   await this.invoiceDetailRepository.update(
  //   //     invoiceDetail.id, // ควรใช้ id ของ invoiceDetail ในการอัปเดต
  //   //     updateIVDetail,
  //   //   );
  //   // }
  //   // await this.invoiceRepository.update(id, updateInvoice);
  //   // return this.invoiceRepository.findOne({
  //   //   where: { id },
  //   //   relations: ['branch', 'employee', 'invoiceDetails'],
  //   // });
  //   return 'not use Return';
  // }

  async remove(invoiceId: number) {
    const removeInvoice = await this.invoiceRepository.findOneBy({
      invoiceId: invoiceId,
    });
    return this.invoiceRepository.remove(removeInvoice);
  }
}
