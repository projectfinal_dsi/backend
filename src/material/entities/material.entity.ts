// import { Branch } from 'src/branch/entities/branch.entity';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
// import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
@Entity()
export class Material {
  @PrimaryGeneratedColumn()
  materialId: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  minimum: number;

  @Column()
  unit: string;

  //   @OneToMany(() => Branch, (branch) => branch.material)
  //   branch: Branch[];
}
