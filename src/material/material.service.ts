import { Injectable } from '@nestjs/common';
import { CreateMaterialDto } from './dto/create-material.dto';
import { UpdateMaterialDto } from './dto/update-material.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Material } from './entities/material.entity';

@Injectable()
export class MaterialService {
  constructor(
    @InjectRepository(Material)
    private MaterialRepository: Repository<Material>,
  ) {}
  async create(createMaterialDto: CreateMaterialDto) {
    const material = new Material();
    material.name = createMaterialDto.name;
    material.price = createMaterialDto.price;
    material.minimum = createMaterialDto.minimum;
    material.unit = createMaterialDto.unit;
    return this.MaterialRepository.save(createMaterialDto);
  }

  findAll() {
    return this.MaterialRepository.find();
  }

  findOne(id: number) {
    return this.MaterialRepository.findOneBy({ materialId: id });
  }

  async update(materialId: number, updateMaterialDto: UpdateMaterialDto) {
    await this.MaterialRepository.update(materialId, updateMaterialDto);
    const Material = await this.MaterialRepository.findOneBy({ materialId });
    return Material;
  }

  async remove(materialId: number) {
    const deleteCustomer = await this.MaterialRepository.findOneBy({
      materialId,
    });
    return this.MaterialRepository.remove(deleteCustomer);
  }
}
