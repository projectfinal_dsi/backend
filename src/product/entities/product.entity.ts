import { Productcategory } from 'src/productcategory/entities/productcategory.entity';
import { ReceiptDetail } from 'src/receipt/entities/receiptDetail.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  productId: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @ManyToOne(() => Productcategory, (pd) => pd.products)
  category: Productcategory;

  @Column({ nullable: true })
  qty?: number;

  @ManyToOne(() => ReceiptDetail, (receiptDetails) => receiptDetails.product)
  receiptDetails: ReceiptDetail[];
}
