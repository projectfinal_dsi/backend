import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from './entities/product.entity';
import { DeepPartial, Repository } from 'typeorm';
import { Productcategory } from 'src/productcategory/entities/productcategory.entity';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(Product)
    private productRepository: Repository<Product>,
    @InjectRepository(Productcategory)
    private productcategoryRepository: Repository<Productcategory>,
  ) {}
  async create(createProductDto: CreateProductDto) {
    const product = new Product();
    const category = await this.productcategoryRepository.findOneBy({
      productcategoryId: createProductDto.category.categoryId,
    });
    product.name = createProductDto.name;
    product.price = createProductDto.price;
    product.category = category;
    product.qty = createProductDto.qty;
    return this.productRepository.save(product);
  }

  findAll() {
    return this.productRepository.find({ relations: ['category'] });
  }

  findOne(productId: number) {
    return this.productRepository.findOne({
      where: { productId },
      relations: ['category'],
    });
  }

  async update(productId: number, updateProductDto: UpdateProductDto) {
    const updateProduct: DeepPartial<Product> = {
      name: updateProductDto.name,
      price: updateProductDto.price,
      qty: updateProductDto.qty,
    };
    // ตรวจสอบว่า updateProductDto.category ไม่เป็น undefined ก่อนที่จะใช้
    if (updateProductDto.category && updateProductDto.category.categoryId) {
      const category = await this.productcategoryRepository.findOne({
        where: { productcategoryId: updateProductDto.category.categoryId },
      });
      // ตรวจสอบว่า category มีค่าไม่เป็น null ก่อนที่จะกำหนดให้กับ updateProduct
      if (category) updateProduct.category = category;
      else
        console.error(
          `Product category with id ${updateProductDto.category.categoryId} not found.`,
        );
    }
    await this.productRepository.update(productId, updateProduct);
    return this.productRepository.findOne({
      where: { productId },
      relations: ['category'],
    });
  }

  async remove(productId: number) {
    const removeProduct = await this.productRepository.findOne({
      where: { productId },
    });
    return this.productRepository.remove(removeProduct);
  }
}
