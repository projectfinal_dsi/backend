import { Product } from 'src/product/entities/product.entity';
import { Promotion } from 'src/promotion/entities/promotion.entity';
import {
  Column,
  Entity,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Productcategory {
  @PrimaryGeneratedColumn()
  productcategoryId: number;

  @Column()
  name: string;

  @ManyToOne(() => Product, (product) => product.category)
  products: Product;

  @ManyToMany(() => Promotion, (promotion) => promotion.limitProductCategory)
  promotions: Promotion[];
}
