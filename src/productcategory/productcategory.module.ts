import { Module } from '@nestjs/common';
import { ProductcategoryService } from './productcategory.service';
import { ProductcategoryController } from './productcategory.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Productcategory } from './entities/productcategory.entity';
import { Product } from 'src/product/entities/product.entity';
import { Promotion } from 'src/promotion/entities/promotion.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Productcategory, Product, Promotion])],
  controllers: [ProductcategoryController],
  providers: [ProductcategoryService],
})
export class ProductcategoryModule {}
