import { Injectable } from '@nestjs/common';
import { CreateProductcategoryDto } from './dto/create-productcategory.dto';
import { UpdateProductcategoryDto } from './dto/update-productcategory.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Productcategory } from './entities/productcategory.entity';

@Injectable()
export class ProductcategoryService {
  constructor(
    @InjectRepository(Productcategory)
    private productcategoryRepository: Repository<Productcategory>,
  ) {}
  create(createProductcategoryDto: CreateProductcategoryDto) {
    return this.productcategoryRepository.save(createProductcategoryDto);
  }

  findAll() {
    return this.productcategoryRepository.find();
  }

  findOne(productcategoryId: number) {
    return this.productcategoryRepository.findOne({
      where: { productcategoryId },
    });
  }

  async update(
    productcategoryId: number,
    updateProductcategoryDto: UpdateProductcategoryDto,
  ) {
    await this.productcategoryRepository.update(
      productcategoryId,
      updateProductcategoryDto,
    );
    return this.productcategoryRepository.findOneBy({ productcategoryId });
  }

  async remove(productcategoryId: number) {
    const removeProductcategory = await this.productcategoryRepository.findOne({
      where: { productcategoryId },
    });
    return this.productcategoryRepository.remove(removeProductcategory);
  }
}
