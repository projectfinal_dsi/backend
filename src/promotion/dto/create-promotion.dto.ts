import { IsNotEmpty, IsOptional } from 'class-validator';
export class CreatePromotionDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  startDate: string;

  @IsNotEmpty()
  endDate: string;

  @IsNotEmpty()
  status: number;

  @IsOptional()
  discountPercentage: number;

  @IsOptional()
  discountBaht: number;

  @IsOptional()
  usePoint: number;

  @IsOptional()
  limitMenu: number;

  @IsNotEmpty()
  limitProductCategory: { productCategoryId: number[] };
}
