import { Productcategory } from 'src/productcategory/entities/productcategory.entity';
import { Receipt } from 'src/receipt/entities/receipt.entity';
import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Promotion {
  @PrimaryGeneratedColumn()
  promotionId: number;

  @Column()
  name: string;

  @Column()
  startDate: string;

  @Column()
  endDate: string;

  @Column()
  status: number;

  @Column({ nullable: true })
  discountPercentage: number;

  @Column({ nullable: true })
  discountBaht: number;

  @Column({ nullable: true })
  usePoint: number;

  @Column({ nullable: true })
  limitMenu: number;

  @ManyToMany(() => Productcategory, (pc) => pc.promotions)
  @JoinTable()
  limitProductCategory: Productcategory[];

  @OneToMany(() => Receipt, (receipt) => receipt.promotion)
  receipt: Receipt[];
}
