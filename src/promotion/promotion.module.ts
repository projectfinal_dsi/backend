import { Module } from '@nestjs/common';
import { PromotionService } from './promotion.service';
import { PromotionController } from './promotion.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Promotion } from './entities/promotion.entity';
import { Productcategory } from 'src/productcategory/entities/productcategory.entity';
import { Receipt } from 'src/receipt/entities/receipt.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Promotion, Productcategory, Receipt])],
  controllers: [PromotionController],
  providers: [PromotionService],
})
export class PromotionModule {}
