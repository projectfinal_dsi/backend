import { Injectable } from '@nestjs/common';
import { CreatePromotionDto } from './dto/create-promotion.dto';
import { UpdatePromotionDto } from './dto/update-promotion.dto';
import { Promotion } from './entities/promotion.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Productcategory } from 'src/productcategory/entities/productcategory.entity';
import { DeepPartial, Repository } from 'typeorm';

@Injectable()
export class PromotionService {
  constructor(
    @InjectRepository(Promotion)
    private promotionRepository: Repository<Promotion>,
    @InjectRepository(Productcategory)
    private productcategoryRepository: Repository<Productcategory>,
  ) {}
  async create(createPromotionDto: CreatePromotionDto) {
    const promotion = new Promotion();
    promotion.name = createPromotionDto.name;
    promotion.startDate = createPromotionDto.startDate;
    promotion.endDate = createPromotionDto.endDate;
    promotion.status = createPromotionDto.status;
    promotion.discountPercentage = createPromotionDto.discountPercentage;
    promotion.discountBaht = createPromotionDto.discountBaht;
    promotion.usePoint = createPromotionDto.usePoint;
    promotion.limitMenu = createPromotionDto.limitMenu;
    promotion.limitProductCategory = [];
    for (const categoryId of createPromotionDto.limitProductCategory
      .productCategoryId) {
      const category = await this.productcategoryRepository.findOneBy({
        productcategoryId: categoryId,
      });
      if (category) {
        promotion.limitProductCategory.push(category);
      } else {
        console.log('Error');
      }
    }

    return this.promotionRepository.save(promotion);
  }

  findAll() {
    return this.promotionRepository.find({
      relations: ['limitProductCategory'],
    });
  }

  findOne(promotionId: number) {
    return this.promotionRepository.findOne({
      where: { promotionId },
      relations: ['promotionRepository'],
    });
  }

  async update(promotionId: number, updatePromotionDto: UpdatePromotionDto) {
    const updatePromotion: DeepPartial<Promotion> = {
      promotionId: promotionId,
      name: updatePromotionDto.name,
      startDate: updatePromotionDto.startDate,
      endDate: updatePromotionDto.endDate,
      status: updatePromotionDto.status,
      usePoint: updatePromotionDto.usePoint,
      discountBaht: updatePromotionDto.discountBaht,
      discountPercentage: updatePromotionDto.discountPercentage,
      limitMenu: updatePromotionDto.limitMenu,
      limitProductCategory: [],
    };
    for (const categoryId of updatePromotionDto.limitProductCategory
      .productCategoryId) {
      const category = await this.productcategoryRepository.findOneBy({
        productcategoryId: categoryId,
      });
      if (category) {
        updatePromotion.limitProductCategory.push(category);
      }
    }
    return this.promotionRepository.findOne({
      where: { promotionId },
      relations: ['limitProductCategory'],
    });
  }

  async remove(promotionId: number) {
    const removePromotion = await this.promotionRepository.findOne({
      where: { promotionId },
    });
    return this.promotionRepository.remove(removePromotion);
  }
}
