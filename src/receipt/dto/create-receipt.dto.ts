import { IsNotEmpty, IsOptional } from 'class-validator';

export class CreateReceiptDto {
  @IsNotEmpty()
  queue: number;

  @IsNotEmpty()
  date: string;

  @IsNotEmpty()
  totalBefore: number;

  @IsNotEmpty()
  discount: number;

  @IsNotEmpty()
  total: number;

  @IsNotEmpty()
  receiveAmount: number;

  @IsNotEmpty()
  change: number;

  @IsNotEmpty()
  payment: string;

  @IsNotEmpty()
  getPoint: number;

  @IsNotEmpty()
  customerPointBefore: number;

  @IsNotEmpty()
  employee: { employeeId: number };

  @IsOptional()
  promotion: { promotionId: number };

  @IsOptional()
  customer: { customerId: number };

  @IsNotEmpty()
  branch: { branchId: number };

  @IsNotEmpty()
  receiptDetails: {
    receitpDetailId?: number;
    qty: number;
    price: number;
    total: number;
    productName: string;
    productSubCategory: number;
    productSweetLevel: number;
    product: { productId: number };
  }[];
}
