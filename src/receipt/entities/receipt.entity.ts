import { Branch } from 'src/branch/entities/branch.entity';
import { Customer } from 'src/customer/entities/customer.entity';
import { Employee } from 'src/employee/entities/employee.entity';
import { Promotion } from 'src/promotion/entities/promotion.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { ReceiptDetail } from './receiptDetail.entity';

@Entity()
export class Receipt {
  @PrimaryGeneratedColumn()
  recieptId: number;

  @Column()
  queue: number;

  @Column()
  date: string;

  @Column()
  totalBefore: number;

  @Column()
  discount: number;

  @Column()
  total: number;

  @Column()
  receiveAmount: number;

  @Column()
  change: number;

  @Column()
  payment: string;

  @Column()
  getPoint: number;

  @Column()
  customerPointBefore: number;

  @ManyToOne(() => Employee, (employee) => employee.receipt)
  employee: Employee;

  @ManyToOne(() => Promotion, (promotion) => promotion.receipt)
  promotion: Promotion;

  @ManyToOne(() => Branch, (branch) => branch.receipt)
  branch: Branch;

  @ManyToOne(() => Customer, (customer) => customer.receipt)
  customer: Customer;

  @OneToMany(() => ReceiptDetail, (receiptDetail) => receiptDetail.receipt)
  receiptDetails: ReceiptDetail[];
}
