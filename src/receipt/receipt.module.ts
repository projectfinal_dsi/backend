import { Module } from '@nestjs/common';
import { ReceiptService } from './receipt.service';
import { ReceiptController } from './receipt.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Receipt } from './entities/receipt.entity';
import { Employee } from 'src/employee/entities/employee.entity';
import { Promotion } from 'src/promotion/entities/promotion.entity';
import { Branch } from 'src/branch/entities/branch.entity';
import { Customer } from 'src/customer/entities/customer.entity';
import { Product } from 'src/product/entities/product.entity';
import { ReceiptDetail } from './entities/receiptDetail.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Receipt,
      Employee,
      Promotion,
      Branch,
      Customer,
      Product,
      ReceiptDetail,
    ]),
  ],
  controllers: [ReceiptController],
  providers: [ReceiptService],
})
export class ReceiptModule {}
