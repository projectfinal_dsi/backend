import { Injectable } from '@nestjs/common';
import { CreateReceiptDto } from './dto/create-receipt.dto';
import { UpdateReceiptDto } from './dto/update-receipt.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Receipt } from './entities/receipt.entity';
import { Repository } from 'typeorm';
import { Employee } from 'src/employee/entities/employee.entity';
import { ReceiptDetail } from './entities/receiptDetail.entity';
import { Branch } from 'src/branch/entities/branch.entity';
import { Customer } from 'src/customer/entities/customer.entity';
import { Promotion } from 'src/promotion/entities/promotion.entity';
import { Product } from 'src/product/entities/product.entity';

@Injectable()
export class ReceiptService {
  constructor(
    @InjectRepository(Receipt)
    private receiptRepository: Repository<Receipt>,
    @InjectRepository(ReceiptDetail)
    private receiptDetailRepository: Repository<ReceiptDetail>, // เพิ่ม ReceiptDetailRepository ที่นี่
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
    @InjectRepository(Customer)
    private customerRepository: Repository<Customer>,
    @InjectRepository(Branch)
    private branchRepository: Repository<Branch>,
    @InjectRepository(Promotion)
    private promotionRepository: Repository<Promotion>,
    @InjectRepository(Product)
    private productRepository: Repository<Product>,
  ) {}
  async create(createReceiptDto: CreateReceiptDto) {
    const receipt = new Receipt();
    receipt.queue = createReceiptDto.queue;
    receipt.date = createReceiptDto.date;
    receipt.totalBefore = createReceiptDto.totalBefore;
    receipt.discount = createReceiptDto.discount;
    receipt.total = createReceiptDto.total;
    receipt.receiveAmount = createReceiptDto.receiveAmount;
    receipt.change = createReceiptDto.change;
    receipt.payment = createReceiptDto.payment;
    receipt.getPoint = createReceiptDto.getPoint;
    receipt.customerPointBefore = createReceiptDto.customerPointBefore;

    const employee = await this.employeeRepository.findOneBy({
      employeeId: createReceiptDto.employee.employeeId,
    });
    receipt.employee = employee;

    const promotion = await this.promotionRepository.findOneBy({
      promotionId: createReceiptDto.promotion.promotionId,
    });
    receipt.promotion = promotion;

    const customer = await this.customerRepository.findOneBy({
      customerId: createReceiptDto.customer.customerId,
    });
    receipt.customer = customer;

    const branch = await this.branchRepository.findOneBy({
      branchId: createReceiptDto.branch.branchId,
    });
    receipt.branch = branch;

    receipt.receiptDetails = [];
    for (const rcDetail of createReceiptDto.receiptDetails) {
      const rcDetailDB = new ReceiptDetail();
      rcDetailDB.qty = rcDetail.qty;
      rcDetailDB.price = rcDetail.price;
      rcDetailDB.total = rcDetail.total;
      rcDetailDB.productName = rcDetail.productName;
      rcDetailDB.productSubCategory = rcDetail.productSubCategory;
      rcDetailDB.productSweetLevel = rcDetail.productSweetLevel;
      const product = await this.productRepository.findOneBy({
        productId: rcDetail.product.productId,
      });
      rcDetailDB.product = product;
      await this.receiptDetailRepository.save(rcDetailDB);
      receipt.receiptDetails.push(rcDetailDB);
    }

    return this.receiptRepository.save(receipt);
  }

  findAll() {
    return this.receiptRepository.find({
      relations: [
        'employee',
        'promotion',
        'branch',
        'customer',
        'receiptDetails',
      ],
    });
  }

  findOne(receiptId: number) {
    return this.receiptRepository.findOne({
      where: { recieptId: receiptId },
      relations: [
        'employee',
        'promotion',
        'branch',
        'customer',
        'receiptDetails',
      ],
    });
  }

  update(id: number, updateReceiptDto: UpdateReceiptDto) {
    return `This action updates a #${id} receipt`;
  }

  async remove(receiptId: number) {
    const removeReceipt = await this.receiptRepository.findOne({
      where: { recieptId: receiptId },
    });
    return this.receiptRepository.remove(removeReceipt);
  }
}
